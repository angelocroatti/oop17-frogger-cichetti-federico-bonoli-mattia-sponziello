package model.world;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import model.gameobject.CentersIterator;
import model.gameobject.GameObject;
import model.gameobject.ObstacleBaseType;
import model.gameobject.ObstaclePostionStrategy;
import model.gameobject.TurtlePositionStrategy;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.lane.EndLane;
import model.lane.Lane;
import model.lane.LaneType;
import model.lane.LaneWithObstacles;
import model.lane.SafeLane;
import utilities.Constants;

/**
 *
 */
public final class WorldImpl implements World { //NOPMD

    /* 
     * Suppressed warning "Class cannot be instantiated and does not provide any static methods or fields"
     * This is not correct because there's a public constructor so class can be instantiated.
     */

    private static final int FIRST_RIVER_LANE = 8;
    private static final int LAST_STREET_LANE = 6;

    private final List<Lane> lanes;

    /**
     * 
     * @param lanes of the world.
     * @param obstacleReferences are the turtles that can go down.
     */
    private WorldImpl(final List<Lane> lanes) {
        this.lanes = Collections.unmodifiableList(lanes);
    }


    /**
     * This is a pattern Builder for the class world.
     * The correct way to build world is:
     * 1 safe lane
     * 5 street
     * 1 safe lane
     * 5 river
     * 1 end lane.
     */
    public static class Builder {

        private static final String SAFE_LANE_EXCEPTION_MESSAGE = "Safe lane can only be in 0 and 6th position.";
        private static final String END_LANE_EXCEPTION_MESSAGE = "End lane can only be in 12th position.";
        private static final String STREET_LANE_POSITION_EXCEPTION_MESSAGE = "Street lane can only be in position 1, 2, 3, 4, 5.";
        private static final String STREET_LANE_OBSTACLES_EXCEPTION_MESSAGE = "Street lane can only contains vehicles.";
        private static final String RIVER_LANE_POSITION_EXCEPTION_MESSAGE = "River lane can only be in position 7, 8, 9, 10, 11.";
        private static final String RIVER_LANE_OBSTACLES_EXCEPTION_MESSAGE = "With this method you can only add logs in river lane.";
        private static final String RIVER_LANE_TURTLES_EXCEPTION_MESSAGE = "Turtle groups can only be of 2, 3 or 4 turtles.";
        private static final String WORLD_UNCORRECT_LANES_NUMBER = "World must have " + Constants.WORLD_NUMBER_OF_LANE + " lanes.";
        private static final int TURTLE_MIN_GROUP_NUMBER = 2;
        private static final int TURTLE_MAX_GROUP_NUMBER = 4;

        private final List<Lane> lanes = new LinkedList<>();
        private CentersIterator obstacleStrategy;

        /**
         * Add a lane without obstacles in the world.
         * @return the builder.
         */
        public Builder addSafeLane() {
            if (this.lanes.size() != 0 && this.lanes.size() != LAST_STREET_LANE) {
                throw new UnsupportedOperationException(SAFE_LANE_EXCEPTION_MESSAGE);
            }
            this.lanes.add(new SafeLane());
            return this;
        }

        /**
         * Add a lane the last lane.
         * @return the builder.
         */
        public Builder addEndLane() {
            if (this.lanes.size() != Constants.WORLD_NUMBER_OF_LANE - 1) {
                throw new UnsupportedOperationException(END_LANE_EXCEPTION_MESSAGE);
            }
            this.lanes.add(new EndLane());
            return this;
        }

        /**
         * Add a street in the world, only vehicles can pass through the street.
         * 
         * @param speed is the velocity of obstacles.
         * @param obstacleType is the type of obstacles.
         * @param numObstacle is the number of obstacles.
         * @return the builder.
         */
        public Builder addStreet(final double speed, final GameObjectType obstacleType, final int numObstacle) {
            if (this.lanes.size() > LAST_STREET_LANE) {
                throw new UnsupportedOperationException(STREET_LANE_POSITION_EXCEPTION_MESSAGE);
            }
            if (obstacleType.getBaseType() != ObstacleBaseType.VEHICLE) {
                throw new IllegalArgumentException(STREET_LANE_OBSTACLES_EXCEPTION_MESSAGE);
            }
            this.lanes.add(new LaneWithObstacles(this.createObstacleSet(obstacleType, numObstacle), speed, LaneType.STREET));
            return this;
        }

        /**
         * Add a river with logs in the world.
         * 
         * @param speed is the velocity of obstacles.
         * @param obstacleType is the type of obstacles.
         * @param numObstacle is the number of obstacles.
         * @return the builder.
         */
        public Builder addRiver(final double speed, final GameObjectType obstacleType, final int numObstacle) {
            if (this.lanes.size() < FIRST_RIVER_LANE - 1) {
                throw new UnsupportedOperationException(RIVER_LANE_POSITION_EXCEPTION_MESSAGE);
            }
            if (obstacleType.getBaseType() != ObstacleBaseType.LOG) {
                throw new IllegalArgumentException(RIVER_LANE_OBSTACLES_EXCEPTION_MESSAGE);
            }
            this.lanes.add(new LaneWithObstacles(this.createObstacleSet(obstacleType, numObstacle), speed, LaneType.RIVER));
            return this;
        }

        /**
         * Add a river with turtles.
         * @param speed is the velocity of turtles.
         * @param grouppedBy is the number of turtles in a group.
         * @return the builder.
         */
        public Builder addRiverWithTurtle(final double speed, final int grouppedBy) {
            if (grouppedBy > TURTLE_MAX_GROUP_NUMBER || grouppedBy < TURTLE_MIN_GROUP_NUMBER) {
                throw new IllegalArgumentException(RIVER_LANE_TURTLES_EXCEPTION_MESSAGE);
            }
            this.lanes.add(new LaneWithObstacles(this.createTurtleSet(grouppedBy), speed, LaneType.RIVER));
            return this;
        }

        /**
         * @return the class World builded.
         */
        public World build() {
            if (this.lanes.size() != Constants.WORLD_NUMBER_OF_LANE) {
                throw new UnsupportedOperationException(WORLD_UNCORRECT_LANES_NUMBER);
            } else {
                return new WorldImpl(this.lanes);
            }
        }

        /**
         * Creates an obstacle set with the following parameters.
         * @param obstacleType
         * @param numObstacle
         * @return the obstacle set.
         */
        private List<GameObject> createObstacleSet(final GameObjectType obstacleType, final int numObstacle) {
            this.obstacleStrategy = new ObstaclePostionStrategy(obstacleType);
            final List<GameObject> l = new LinkedList<>();
            for (int i = 0; i < numObstacle && obstacleStrategy.hasNext(); i++) {
                l.add(obstacleType.create(obstacleStrategy.next()));
            }
            return l;
        }

        /**
         * Create a turtle set with the following parameters.
         * @param groupBy is the number of turtle in a group.
         * @return the obstacle set.
         */
        private List<GameObject> createTurtleSet(final int groupBy) {
            this.obstacleStrategy = new TurtlePositionStrategy(groupBy);
            final List<GameObject> l = new LinkedList<>();
            while (obstacleStrategy.hasNext()) {
                l.add(GameObjectType.TURTLE.create(obstacleStrategy.next()));
            }
            return l;
        }

    }

    /**
     * 
     */
    @Override
    public List<Lane> getLane() {
        return this.lanes;
    }

    /**
     * 
     */
    @Override
    public List<GameObject> getDen() {
        return this.lanes.get(Constants.WORLD_NUMBER_OF_LANE - 1).getObstacle();
    }

}
