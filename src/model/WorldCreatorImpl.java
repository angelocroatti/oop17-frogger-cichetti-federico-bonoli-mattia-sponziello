package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import model.gameobject.ObstacleBaseType;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.world.World;
import model.world.WorldImpl;
import utilities.Constants;

/**
 * world creator.
 */
public class WorldCreatorImpl implements WorldCreator {

    private static final int MIN_SPEED = 10;
    private static final int MAX_SPEED = 20;
    private static final int MIN_OBST = 2;
    private static final int MAX_OBST = 6;
    private static final int MAX_TURTLE = 3;
    private final Random rand;
    private final List<GameObjectType> riverObst;
    private final List<GameObjectType> roadObst;
    private int ups;
    private GameDifficult diff = Constants.DIFF_DEFAULT;
    private boolean direction;

    /**
     * constructor.
     */
    public WorldCreatorImpl() {
        rand = new Random();
        riverObst = new ArrayList<>();
        roadObst = new ArrayList<>();
        direction = false;
        for (final GameObjectType t : GameObjectType.values()) {
            if (t.getBaseType() == ObstacleBaseType.LOG) {
                riverObst.add(t);
            } else if (t.getBaseType() == ObstacleBaseType.VEHICLE) {
                roadObst.add(t);
            }
        }
        direction = rand.nextBoolean();
    }

    /**
     * creates a new randomic world.
     * @return new world.
     */
    public World createWorld() {
        final WorldImpl.Builder builder = new WorldImpl.Builder();
        builder.addSafeLane();
        Collections.shuffle(roadObst);
        roadObst.stream().limit(Constants.WORLD_NUMBER_OF_ROADS).forEach(o -> builder.addStreet(getRandomSpeed(), o, (int) (getRandomInRange(MIN_OBST, MAX_OBST) * diff.getRoadObstacleMultiplier())));
        builder.addSafeLane();
        builder.addRiver(getRandomSpeed(),  riverObst.get(rand.nextInt(riverObst.size())), (int) (getRandomInRange(MIN_OBST, MAX_OBST) * diff.getRiverObstacleMultiplier()))
        .addRiverWithTurtle(getRandomSpeed(), getRandomInRange(MIN_OBST, MAX_TURTLE))
        .addRiver(getRandomSpeed(),  riverObst.get(rand.nextInt(riverObst.size())), (int) (getRandomInRange(MIN_OBST, MAX_OBST) * diff.getRiverObstacleMultiplier()))
        .addRiverWithTurtle(getRandomSpeed(), getRandomInRange(MIN_OBST, MAX_TURTLE))
        .addRiver(getRandomSpeed(),  riverObst.get(rand.nextInt(riverObst.size())), (int) (getRandomInRange(MIN_OBST, MAX_OBST) * diff.getRiverObstacleMultiplier()))
        .addEndLane();
        return builder.build();
    }

    /**
     * set difficult to calculate world creation.
     * @param d difficult
     */
    @Override
    public void setDifficult(final GameDifficult d) {
        this.diff = d;
    }

    /**
     * set the game updates per second to calculate appropriate obstacle speed.
     * @param ups frequence of updates
     */
    @Override
    public void setUPS(final int ups) {
        this.ups = ups;
    }

    /**
     * return a random number in range.
     * @param lower bound
     * @param upper bound
     * @return random in range
     */
    private int getRandomInRange(final int lower, final int upper) {
        return rand.nextInt((upper - lower) + 1) + lower;
    }

    /**
     * generate a random speed: the sign determines the direction of movement.
     * every time the function is called it change direction.
     * it uses ups to determine the amount of space to advance per tick.
     * @return random double
     */
    private double getRandomSpeed() {
        direction = !direction;
        return getRandomInRange(MIN_SPEED, MAX_SPEED) * (direction ? -1 : 1) * (1d / ups) * diff.getSpeedMultiplier();
    }

}
