package model;

import model.world.World;

/**
 * factory responsible for world building.
 */
public interface WorldCreator {

    /**
     * create a new world based on setted parameters.
     * @return new world.
     */
    World createWorld();

    /**
     * set difficult, for multiplier.
     * @param d difficult
     */
    void setDifficult(GameDifficult d);

    /**
     * set the updates per second calculated.
     * @param ups to be used in speed calculation.
     */
    void setUPS(int ups);

}
