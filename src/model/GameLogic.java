package model;

import java.util.Optional;

import controller.movement.Invoker;
import javafx.scene.input.KeyCode;
import model.frog.Frog;
import model.score.ScoreManager;
import model.world.World;
import utilities.GameObserver;

/**
 * public interface for game logic class.
 */
public interface GameLogic {

    /**
     * initialize game: create the starting world, set the frog position and the timer.
     * @param ups to be displayed
     */
    void initializeGame(int ups);

    /**
     * updates all game objects, to be called every frame in gameloop.
     */
    void update();

    /**
     * check if game is over.
     * @return true if game is ended or false otherwise
     */
    boolean gameOver();

    /**
     * get the user input.
     * @param input received
     */
    void registerInput(Optional<KeyCode> input);

    /**
     * decrease time of toDec sec.
     * @param toDec seconds to remove to the timer
     */
    void advanceTime(int toDec);

    /**
     * getter for remaining time.
     * @return time left
     */
    int getTime();

    /**
     * set difficult for world building.
     * @param d difficult
     */
    void setDifficult(GameDifficult d);

    /**
     * Default getter for the World.
     * @return world
     */
    World getWorld();

    /**
     * Default getter for the Frog.
     * @return frog
     */
    Frog getFrog();

    /**
     * Default getter for the ScoreManager.
     * @return scoreManager
     */
    ScoreManager getScoreManager();

    /**
     * Default getter for the difficulty.
     * @return difficult
     */
    GameDifficult getDifficult();

    /**
     * Default getter for the InputHandler.
     * @return InputHandler
     */
    Invoker getInputHandler();

    /**
     * Adds an Observer to the GameLogic (may be used for collisions).
     * @param g GameObserver 
     */
    void registerObserver(GameObserver g);

    /**
     * Adds an Observer for the advanceLevel method only.
     * @param g GameObserver 
     */
    void registerAdvancedObserver(GameObserver g);

    /**
     * Detaches all registered observers in this GameLogic.
     */
    void detachObservers();
}
