package model.mod;

import model.gameobject.GameObject;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.gameobject.ObstacleBaseType;

/**
 *
 */
public class ModEntity implements ModObstacle {

    private final ModType modType;
    private final GameObject base;

    /**
     * Constructor for the ModEntity.
     * @param base The underlying GameObject.
     * @param type The type of the mod.
     */
    public ModEntity(final GameObject base, final ModType type) {
        this.modType = type;
        this.base = base;
    }

    /**/
    @Override
    public double getCenter() {
        return base.getCenter();
    }

    @Override
    public void setCenter(final double newCenter) {
        //mods do not move
    }

    /**/
    @Override
    public GameObjectType getGameObjectType() {
        return base.getGameObjectType();
    }

    /**/
    @Override
    public double getWidth() {
        return base.getWidth();
    }

    /**/
    @Override
    public ObstacleBaseType getBaseType() {
        return base.getBaseType();
    }

    /**/
    @Override
    public ModType getModType() {
        return this.modType;
    }

    /**/
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((base == null) ? 0 : base.hashCode());
        result = prime * result + ((modType == null) ? 0 : modType.hashCode());
        return result;
    }

    /**/
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModEntity other = (ModEntity) obj;
        if (base == null) {
            if (other.base != null) {
                return false;
            }
        } else if (!base.equals(other.base)) {
            return false;
        }
        return modType == other.modType;
    }
}
