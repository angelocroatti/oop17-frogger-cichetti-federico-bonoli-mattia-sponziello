package model.mod;

/**
 * Exception thrown when a Mod is added to a Lane with another Mod already present
 * in its Obstacle set.
 */
public class ModAlreadyPresentException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -2335689489713681803L;

    /**
     * Default exception constructor.
     */
    public ModAlreadyPresentException() {
        super();
    }

    /**
     * Constructor with error message.
     * @param s The error message
     */
    public ModAlreadyPresentException(final String s) {
        super(s);
    }

}
