package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.gameobject.GameObjectImpl.GameObjectType;
import model.lane.LaneType;
import model.world.World;
import model.world.WorldImpl;
import utilities.Constants;

/**
 * JUnit tests for world.
 */
public class TestsWorld {

    private static final String MESSAGE_SAME = "Should be the same";
    private static final String MESSAGE_TRUE = "Should be the true";
    private static final String MESSAGE_FALSE = "Should be the false";

    private static final int SIXTH_LANE = 6;
    private static final int SENVENTH_LANE = 7;

    /**
     * WorldImpl tests.
     */
    @Test
    public void testWorldCreation() {
        final World world = new WorldImpl.Builder().addSafeLane()
                                                   .addStreet(2, GameObjectType.PURPLE_CAR, 4)
                                                   .addStreet(4, GameObjectType.RACE_CAR, 2)
                                                   .addStreet(2, GameObjectType.WHITE_CAR, 3)
                                                   .addStreet(2, GameObjectType.TRUCK, 3)
                                                   .addStreet(2, GameObjectType.TRUCK, 3)
                                                   .addSafeLane()
                                                   .addRiver(3, GameObjectType.SMALL_LOG, 4)
                                                   .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
                                                   .addRiver(4, GameObjectType.SMALL_LOG, 4)
                                                   .addRiver(3, GameObjectType.BIG_LOG, 1)
                                                   .addRiver(4, GameObjectType.SMALL_LOG, 4)
                                                   .addEndLane()
                                                   .build();

        // tests for correct lane type
        assertEquals(TestsWorld.MESSAGE_SAME, world.getLane().size(), Constants.WORLD_NUMBER_OF_LANE);
        assertEquals(TestsWorld.MESSAGE_SAME, world.getLane().get(0).getLaneType(), LaneType.SAFE_LANE);
        assertEquals(TestsWorld.MESSAGE_SAME, world.getLane().get(1).getLaneType(), LaneType.STREET);
        assertEquals(TestsWorld.MESSAGE_SAME, world.getLane().get(TestsWorld.SIXTH_LANE).getLaneType(), LaneType.SAFE_LANE);
        assertEquals(TestsWorld.MESSAGE_SAME, world.getLane().get(TestsWorld.SENVENTH_LANE).getLaneType(), LaneType.RIVER);
        assertEquals(TestsWorld.MESSAGE_SAME, world.getLane().get(Constants.WORLD_NUMBER_OF_LANE - 1).getLaneType(), LaneType.END_LANE);

        // tests for correct obstacle set
        assertTrue(TestsWorld.MESSAGE_TRUE, world.getLane().get(0).getObstacle().isEmpty());
        assertFalse(TestsWorld.MESSAGE_FALSE, world.getLane().get(1).getObstacle().isEmpty());
        assertTrue(TestsWorld.MESSAGE_TRUE, world.getLane().get(TestsWorld.SIXTH_LANE).getObstacle().isEmpty());

        // tests check errors of min and max number of lane
        try {
            new WorldImpl.Builder().addSafeLane()
                                .addStreet(2, GameObjectType.PURPLE_CAR, 4)
                                .addStreet(4, GameObjectType.RACE_CAR, 2)
                                .addStreet(2, GameObjectType.WHITE_CAR, 3)
                                .addStreet(2, GameObjectType.TRUCK, 3)
                                .addStreet(2, GameObjectType.TRUCK, 3)
                                .addSafeLane()
                                .addRiver(3, GameObjectType.SMALL_LOG, 4)
                                .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
                                .addRiver(4, GameObjectType.SMALL_LOG, 4)
                                .addRiver(3, GameObjectType.BIG_LOG, 1)
                                .addRiver(4, GameObjectType.SMALL_LOG, 4)
                                .addRiverWithTurtle(2, 2)
                                .addEndLane()
                                .build();
            fail("Can't add more than 13 lane");
        } catch (Exception e) {
            System.out.println("Can't add more than 13 lane");
        }

        // tests check errors of min and max number of lane
        try {
            new WorldImpl.Builder().addSafeLane()
            .addStreet(2, GameObjectType.PURPLE_CAR, 4)
            .addStreet(4, GameObjectType.RACE_CAR, 2)
            .addStreet(2, GameObjectType.WHITE_CAR, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addSafeLane()
            .addRiver(3, GameObjectType.SMALL_LOG, 4)
            .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addRiver(3, GameObjectType.BIG_LOG, 1)
            .addEndLane()
            .build();
            fail("Can't add less than 13 lane");
        } catch (Exception e) {
            System.out.println("Can't add less than 13 lane");
        }

        // tests check errors of obstacles insertion
        try {
            new WorldImpl.Builder().addSafeLane()
            .addStreet(2, GameObjectType.BIG_LOG, 4) // ERROR
            .addStreet(4, GameObjectType.RACE_CAR, 2)
            .addStreet(2, GameObjectType.WHITE_CAR, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addSafeLane()
            .addRiver(3, GameObjectType.SMALL_LOG, 4)
            .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addRiver(3, GameObjectType.BIG_LOG, 1)
            .addRiverWithTurtle(2, 2)
            .addEndLane()
            .build();
            fail("Can't add logs in the street");
        } catch (Exception e) {
            System.out.println("Can't add logs in the street");
        }

        try {
            new WorldImpl.Builder().addSafeLane()
            .addStreet(2, GameObjectType.TRUCK, 4)
            .addStreet(4, GameObjectType.RACE_CAR, 2)
            .addStreet(2, GameObjectType.WHITE_CAR, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addSafeLane()
            .addRiver(3, GameObjectType.TRUCK, 4) // ERROR
            .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addRiver(3, GameObjectType.BIG_LOG, 1)
            .addRiverWithTurtle(2, 2)
            .addEndLane()
            .build();
            fail("Can't add vehicles in the river");
        } catch (Exception e) {
            System.out.println("Can't add vehicles in the river");
        }

    }

}
