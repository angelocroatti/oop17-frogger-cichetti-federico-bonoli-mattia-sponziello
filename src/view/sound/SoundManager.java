package view.sound;

import view.ViewImpl.GameScreen;

/**
 * A class that manages the reproduction of sounds.
 */
public interface SoundManager {

    /**
     * Plays the background sound of this particular GameScreen in a Thread.
     * @param s the GameScreen
     */
    void play(GameScreen s);

    /**
     * Plays a winning fanfare.
     */
    void playWin();

    /**
     * Stops all sounds.
     */
    void end();
}
