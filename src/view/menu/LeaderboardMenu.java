package view.menu;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import view.AbstractScene;
import view.View;
import view.ViewImpl;

/**
 * layout for leaderboard menu.
 */
public class LeaderboardMenu extends AbstractScene {

    private static final int TITLE_FONT_SIZE = 7;
    private static final int SCOREBOX_SPACING = 11;
    private static final int BTNBOX_PADDING = 15;

    private final List<Label> labels;
    private final VBox scoreBox;
    private final View view;
    /**
     * @param width of the scene
     * @param height of the scene
     * @param v view reference
     */
    public LeaderboardMenu(final double width, final double height, final View v) {
        super(new BorderPane(), width, height);
        view = v;
        final BorderPane pane = (BorderPane) getRoot();
        labels = new ArrayList<>();
        final Button back = new Button("MENU");
        final Label title = new Label("LEADERBOARD");
        final Button reset = new Button("RESET");
        scoreBox = new VBox();
        final HBox btnBox = new HBox(back, reset);
        back.prefWidthProperty().bind(this.widthProperty().divide(3.0));
        reset.prefWidthProperty().bind(this.widthProperty().divide(3.0));
        title.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", this.widthProperty().divide(TITLE_FONT_SIZE)));
        scoreBox.spacingProperty().bind(this.heightProperty().divide(SCOREBOX_SPACING));
        btnBox.spacingProperty().bind(this.widthProperty().divide(10));
        btnBox.setPadding(new Insets(0, 0, height / BTNBOX_PADDING, 0));
        scoreBox.getChildren().addAll(labels);
        scoreBox.setAlignment(Pos.CENTER);
        BorderPane.setAlignment(title, Pos.CENTER);
        pane.setTop(title);
        pane.setCenter(scoreBox);
        pane.setBottom(btnBox);
        btnBox.setAlignment(Pos.CENTER);
        BorderPane.setAlignment(btnBox, Pos.BOTTOM_CENTER);
        back.setOnAction(e -> view.changeScene(ViewImpl.GameScreen.MAINMENU));
        reset.setOnAction(e -> {
            view.getController().getLeaderboardManager().resetAllScore();
            Platform.runLater(() -> this.initialize());
        });
    }

    /**
     * scene initialization.
     */
    public void initialize() {
        labels.clear();
        scoreBox.getChildren().clear();
        view.getController().getLeaderboardManager().getScoreList().forEach(s -> labels.add(new Label(s.getX() + " - " + s.getY() + "  PTS")));
        if (labels.isEmpty()) {
            labels.add(new Label("NO SCORE YET"));
        }
        labels.forEach(l -> scoreBox.getChildren().add(l));
    }
}
