package view.menu;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import utilities.Pair;
import view.AbstractScene;
import view.View;
import view.ViewImpl;
/**
 * end menu.
 */
public class EndMenu extends AbstractScene {

    private static final int HEIGHT_DIV = 10;
    private static final int PADDING_HEIGHT = 15;
    private static final int TRANSLATE_X_DIV = 35;
    private static final int TRANSLATE_Y_DIV = 12;
    private static final int WIDTH_DIV = 3;
    private static final int FONT_DIV = 8;

    private int points;
    private final View view;
    private final Label pointsLabel;
    private final HBox inputBox;
    private final HBox bottomBox;
    private final Button save;
    /**
     * @param width of the scene
     * @param height of the scene
     * @param v view reference
     */
    public EndMenu(final double width, final double height, final View v) {
        super(new BorderPane(), width, height);
        final BorderPane pane = (BorderPane) getRoot();
        final ImageView gameOver = new ImageView();
        final TextField username = new TextField();
        final Button leaderboardMenu = new Button("LEADERBOARD");
        final Button mainMenu = new Button("MENU");
        final Button exit = new Button("EXIT");
        view = v;
        pointsLabel = new Label();
        save = new Button("SAVE");
        inputBox = new HBox(username, save);
        bottomBox = new HBox(leaderboardMenu, mainMenu);
        final VBox topBox = new VBox(gameOver, pointsLabel);
        gameOver.setId("gameOverImage");
        gameOver.setFitHeight(height / HEIGHT_DIV);
        gameOver.setFitWidth(width);
        gameOver.setTranslateX(width / TRANSLATE_X_DIV);
        gameOver.setTranslateY(height / TRANSLATE_Y_DIV);
        pane.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", super.getFontSize()));
        pointsLabel.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", this.widthProperty().divide(FONT_DIV)));
        pointsLabel.setPadding(new Insets(height / HEIGHT_DIV, 0, 0, 0));
        mainMenu.prefWidthProperty().bind(this.widthProperty().divide(WIDTH_DIV));
        leaderboardMenu.prefWidthProperty().bind(this.widthProperty().divide(WIDTH_DIV));
        bottomBox.spacingProperty().bind(this.widthProperty().divide(HEIGHT_DIV));
        inputBox.spacingProperty().bind(this.widthProperty().divide(HEIGHT_DIV));
        bottomBox.setPadding(new Insets(0, 0, height / PADDING_HEIGHT, 0));
        pane.setTop(topBox);
        topBox.setAlignment(Pos.BASELINE_CENTER);
        pane.setCenter(inputBox);
        inputBox.setAlignment(Pos.CENTER);
        BorderPane.setAlignment(inputBox, Pos.CENTER);
        pane.setBottom(bottomBox);
        bottomBox.setAlignment(Pos.CENTER);
        bottomBox.setVisible(false);
        leaderboardMenu.setOnAction(e -> view.changeScene(ViewImpl.GameScreen.LEADERBOARD).initialize());
        mainMenu.setOnAction(e -> view.changeScene(ViewImpl.GameScreen.MAINMENU));
        save.setOnAction(e -> {
            if (!username.getText().isEmpty()) {
                bottomBox.setVisible(true);
                view.getController().getLeaderboardManager().addScore(new Pair<>(username.getText(), points));
                save.setDisable(true);
                username.clear();
            }
        });
        exit.setOnAction(e -> Platform.exit());
    }

    /***/
    @Override
    public void initialize() { 
        save.setDisable(false);
        bottomBox.setVisible(false);
        points = view.getController().getLeaderboardManager().getPartialScore();
        pointsLabel.setText(String.valueOf(points));
        if (view.getController().getLeaderboardManager().checkScore(points)) {
            inputBox.setVisible(true);
        } else {
            inputBox.setVisible(false);
            bottomBox.setVisible(true);
        }
    }

}
