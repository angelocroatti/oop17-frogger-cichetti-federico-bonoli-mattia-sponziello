package controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import utilities.Pair;

/**
 * Class to manage the leaderboard.
 */
public class LeaderboardManagerImpl implements LeaderboardManager {

    /**
     * Max number of score to manage.
     */
    public static final int MAX_SCORE_NUMBER = 5;
    /**
     * leaderboard file path.
     */
    public static final String FILE_PATH = System.getProperty("user.dir") + System.getProperty("file.separator") + "leaderboard.dat";

    private final List<Pair<String, Integer>> scoreList;
    private boolean isModified;
    private int partialScore;

    /**
     * public constructor that initialize the object.
     */
    public LeaderboardManagerImpl() {
        scoreList = new LinkedList<>();
        isModified = false;
        readDataFromFile();
    }

    /**
     * @return the score list as UnmodifiableList.
     */
    public List<Pair<String, Integer>> getScoreList() {
        return Collections.unmodifiableList(scoreList);
    }

    /**
     * add new score to the list and remove the last score if are more than MAX_SCORE_NUMBER.
     * @param score to be added.
     */
    public void addScore(final Pair<String, Integer> score) {
        if (scoreList.size() < MAX_SCORE_NUMBER) {
            scoreList.add(score);
            Collections.sort(scoreList, (s1, s2) -> s2.getY() - s1.getY());
        } else {
            scoreList.add(score);
            Collections.sort(scoreList, (s1, s2) -> s2.getY() - s1.getY());
            scoreList.remove(scoreList.size() - 1);
        }
        isModified = true;
    }

    /**
     * empty the record of the scores.
     */
    public void resetAllScore() {
        isModified = true;
        scoreList.clear();
    }
    /**
     * update the leaderboard state to file.
     */
    public void update() {
        if (isModified) {
            try (DataOutputStream d = new DataOutputStream(new FileOutputStream(FILE_PATH))) {
                for (final Pair<String, Integer> s : scoreList) {
                    d.writeUTF(s.getX());
                    d.writeInt(s.getY());
                }
            } catch (IOException e) {
                System.err.println("File not found, creating it...");
            } 
            isModified = false;
        }
    }

    /**
     * check if the new score is in the top MAX_SCORE_NUMBER score.
     * @param s new score to check
     * @return true if could be added in list, false otherwise
     */
    public boolean checkScore(final int s) {
        if (scoreList.isEmpty() || scoreList.size() < MAX_SCORE_NUMBER) {
            return true;
        }
        return s >= scoreList.get(scoreList.size() - 1).getY();
//        if (s >= scoreList.get(scoreList.size() - 1).getY()) {
//            return true;
//        }
//        return false;
    }
    /**
     * @return the last score obtained after game over.
     */
    public int getPartialScore() {
        return partialScore;
    }

    /**
     * save the points obtained, to be memorized in the list is must have an username.
     * @param points obtained
     */
    public void setPartialScore(final int points) {
        this.partialScore = points;
    }

    private void readDataFromFile() {
        try (DataInputStream d = new DataInputStream(new FileInputStream(FILE_PATH))) {
            while (d.available() > 0) {
                scoreList.add(new Pair<String, Integer>(d.readUTF(), d.readInt()));
            }
        } catch (IOException e) {
            System.err.println("File not found, creating it...");
        } 
    }
}
