package controller;

import javafx.application.Application;
import javafx.stage.Stage;
import model.GameLogic;
import model.GameLogicImpl;
import view.View;
import view.ViewImpl;

/**
 * App launcher.
 */
public class ApplicationLauncher extends Application {

    private Controller controller;

    /**
     * Starts the application.
     * @param args additional command-line arguments
     */
    public static void main(final String[] args) {
        launch(args);
    }

    /**
     * JavaFX application starting point.
     */
    @Override
    public void start(final Stage primaryStage) throws Exception {
        final View view = new ViewImpl(primaryStage);
        final GameLogic game = new GameLogicImpl();
        controller = new ControllerImpl(view, game);
        view.setController(controller);
        view.startMenu();
    }
    /**
     * closure action.
     */
    @Override
    public void stop() {
        controller.stopGame();
        controller.getLeaderboardManager().update();
    }
}
