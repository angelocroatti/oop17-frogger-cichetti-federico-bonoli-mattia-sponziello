package controller.movement;

import java.util.Optional;

import model.frog.Frog;
import model.gameobject.GameObject;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.world.World;
import model.lane.Lane;
import model.lane.LaneType;
import model.mod.ModObstacle;

/** 
 * Checks if the frog is colliding with any kind of obstacle.
 */
public class FrogCollision {

    /**
     * The frog the checks are made on.
     */
    private final Frog frog;
    /**
     * The lane the frog is currently in.
     */
    private final Lane currentLane;

    /**
     * @param frog the frog the checks are made on.
     * @param world the world the frog is in.
     */
    public FrogCollision(final Frog frog, final World world) {                                  //World used in the constructor instead of currentlane 
        this.frog = frog;                                                                       //due to ease of use
        this.currentLane = world.getLane().get(frog.getOccupiedLane());
    }

    /**
     * Checks if the frog is hitting a car or is in the river.
     * @return true if the frog is hitting an obstacle or is in the river, false otherwise. 
     */
    public boolean checkCollision() {

        if (currentLane.getLaneType() == LaneType.STREET 
                && currentLane.getObstacle().stream().anyMatch((obs) -> obs.isColliding(frog))) {      //If the frog IS colliding on the STREET, it hit a car
                return true;                                                                    //return TRUE
            }

        if (currentLane.getLaneType() == LaneType.RIVER) {
            boolean onLog = false;
            if (currentLane.getObstacle().stream().anyMatch((obs) -> obs.isColliding(frog))) {  //If the frog IS colliding on the RIVER,it is on a log
                onLog = true;                                                                   //set onLog TRUE
            }
            return (!onLog);                                                                    //return false if frog in on the log, true otherwise
        }

        return false;                                                                           //default to false for lanes with no obstacles
    }

    /**
     * Checks if the frog has safely reached a den.
     * @return optional containing the den the frog is in, empty otherwise.
     */
    public Optional<GameObject> denReached() {

        if (currentLane.getLaneType() == LaneType.END_LANE) {
            return currentLane.getObstacle().stream()                                           //get stream containing empty den hit
                    .filter((obs) -> obs.isColliding(frog) && obs.getGameObjectType() == GameObjectType.EMPTY_DEN)
                    .findAny();                                                                 //return optional of it, empty if none hit
        }

        return Optional.empty();                                                                //defaults to empty optional on all lanes but end lane
    }

    /**
     * Checks if the frog has hit a mod.
     * @return true if the frog hit a mod, NULL otherwise.
     */
    public Optional<ModObstacle> modCollision() {

        final Optional<ModObstacle> modHit = currentLane.getMods().stream().filter((obs) -> obs.isColliding(frog)).findAny();
        if (modHit.isPresent()) {                                                               //If the frog IS colliding a mod on any lane
            currentLane.removeMod(modHit.get());                                                //remove mod from field
            return modHit;                                                                      //return optional of said mod
        }
        return Optional.empty();
    }
}
