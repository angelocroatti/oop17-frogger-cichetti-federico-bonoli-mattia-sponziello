package controller.movement;

import model.frog.Frog;

/**
 * Allows the creation of multiples commands which affect the frog.
 */
public interface Command {
    /**
     * Every command implements his execute method based on the actions it needs to accomplish.
     * @param frog the frog game object to execute the command on
     */
    void execute(Frog frog);
}
