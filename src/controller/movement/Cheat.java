package controller.movement;

import model.frog.Frog;

/**
 * Allows the user to toggle a mode which ignores obstacles collisions, basically cheating.
 */
public class Cheat implements Command {

    /**
     * Toggles a mode which ignores obstacles collisions.
     * @param frog the frog to set 
     */
    public void execute(final Frog frog) {
        frog.toggleCheating();
    }

}
