# README #

Questo repository contiene il progetto per l'esame di OOP per l'anno 2017/2018 degli studenti Cichetti Federico, Bonoli Mattia, Sponziello Nicolò, Pistocchi Filippo.  
Il progetto consiste in una versione moderna del classico gioco arcade Frogger, scritta in Java con l'utilizzo della libreria JavaFX.  
Nel gioco originale, una rana deve attraversare un mondo di gioco con diversi ostacoli e raggiungere la sua tana all'estremità opposta.  
Si è scelto di estendere il gioco originale aggiungendo Mod, una God Mode e diverse difficoltà selezionabili dal menu iniziale.  
La sezione Downloads contiene il .jar eseguibile del progetto e la relazione di gruppo.